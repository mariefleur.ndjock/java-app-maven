FROM openjdk:8-jre-alpine
ADD /target/java-maven-app-*.jar  java-maven-app-*.jar
ENTRYPOINT [ "java" , "-jar" , "java-maven-app-*.jar" ]
EXPOSE 8080
CMD java -jar java-maven-app-*.jar
WORKDIR /usr/app



