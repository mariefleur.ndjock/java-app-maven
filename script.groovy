def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    
    withCredentials([usernamePassword(credentialsId: '74630b3b-8b30-43f4-bdd5-0703db945cca', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
        sh 'docker build -t mariefleur89/java-app-maven:1.1  .'
        sh 'docker login -u $USERNAME -p $PASSWORD'
        sh 'docker push mariefleur89/java-app-maven:1.1'
    }
} 

def deployApp() {
    'echo I am inside deploy application'
} 

return this
